#include <iostream>
#include "Graph.h"

int main(int argc, char* argv[])
{
    Graph* GraphRef = new Graph;
    
    GraphRef->AddNode(0);
    GraphRef->AddNode(1);
    GraphRef->AddNode(2);
    GraphRef->AddNode(3);
    GraphRef->AddNode(4);
    GraphRef->AddNode(5);
    GraphRef->AddNode(6);
    GraphRef->AddNode(7);
    GraphRef->AddNode(8);

    GraphRef->AddVertex(0, 1, 11);
    GraphRef->AddVertex(0, 8, 12);
    GraphRef->AddVertex(0, 7, 13);
    GraphRef->AddVertex(1, 2, 17);
    GraphRef->AddVertex(1, 8, 10);
    GraphRef->AddVertex(2, 8, 5);
    GraphRef->AddVertex(2, 3, 4);
    GraphRef->AddVertex(3, 8, 2);
    GraphRef->AddVertex(3, 4, 3);
    GraphRef->AddVertex(4, 5, 7);
    GraphRef->AddVertex(4, 6, 8);
    GraphRef->AddVertex(4, 8, 6);
    GraphRef->AddVertex(5, 6, 16);
    GraphRef->AddVertex(5, 7, 14);
    GraphRef->AddVertex(6, 8, 1);
    GraphRef->AddVertex(6, 7, 15);
    GraphRef->AddVertex(7, 8, 9);
    
   auto Edges = GraphRef->FindMinSkeleton();
    
    // for (auto It : Edges )
    //     cout << It.Weight << endl;
    
    return 0;
}
