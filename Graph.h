﻿#pragma once
#include <vector>

using namespace std;

struct Edge
{
    int PreviousIndex;
    int TargetIndex;
    int Weight;

    Edge(int PreviousIndex, int TargetIndex, int Weight) : PreviousIndex(PreviousIndex), TargetIndex(TargetIndex), Weight(Weight){};
};

class Node
{
public:

    Node* Parent = nullptr;
    Node() {Parent = this;};
};

class Graph
{
public:
    vector<Node*> Nodes;
    vector<Edge> Vertexes;
    
    Node* AddNode(char NodeName);
    Node* GetNode(int Index);
    void AddVertex(int FirstIndex, int SecondIndex, int Weight);
    
    Node* FindParent(Node* Node);
    void Merge(Node* First, Node* Second);
    vector<Edge> FindMinSkeleton();


};

