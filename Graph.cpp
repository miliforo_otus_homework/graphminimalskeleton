﻿#include "Graph.h"

#include <algorithm>
#include <iostream>
#include "map"

using namespace std;

void Graph::AddVertex(int FirstIndex, int SecondIndex, int Weight)
{
    Edge EdgeRef(FirstIndex, SecondIndex, Weight);
    Vertexes.push_back(EdgeRef);
}

Node* Graph::AddNode(char NodeName)
{
    auto NodeRef = new Node();
    Nodes.push_back(NodeRef);
    return NodeRef;
}

Node* Graph::FindParent(Node* NodeRef)
{
    if (NodeRef->Parent == NodeRef)
        return NodeRef;

    Node* NodeParent = FindParent(NodeRef->Parent);
    return NodeParent;
}

void Graph::Merge(Node* First, Node* Second)
{
    Node* RootFirst = FindParent(First);
    Node* RootSecond = FindParent(Second);

    if (RootFirst == RootSecond)
        return;

    RootFirst->Parent = RootSecond;
}

Node* Graph::GetNode(int Index)
{
    return Nodes.at(Index);
}

bool SortEdgesByWeight(const Edge& First, const Edge& Second)
{
    return First.Weight < Second.Weight;
}

vector<Edge> Graph::FindMinSkeleton()
{

    sort(Vertexes.begin(), Vertexes.end(), SortEdgesByWeight);
    
    vector<Edge> SortVertexes;
    for (auto It : Vertexes)
    {
        if (FindParent(Nodes[It.TargetIndex]) != FindParent(Nodes[It.PreviousIndex]))
        {
            Merge(Nodes[It.TargetIndex], FindParent(Nodes[It.PreviousIndex]));
            SortVertexes.push_back(It);
        }
    }
    return SortVertexes;
}

